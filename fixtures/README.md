identity DB fixtures

tools to create a dataset for test


repeat:
    we create an identity
    for each identity we create
        one or more contract(s)
        with different roles


we identify several profils

SimpleMobileUser:
    1 contract of type MOBILE with roles owner,user,payer
SimpleLegacyUser:
    1 contract of type FIX with roles owner,user,payer
SimpleInternetUser:
    1 contract of type INTERNET with roles owner,user,payer
DoubleUSerMF:
    1 contract of type MOBILE with roles owner,user,payer
    1 contract of type FIX with roles owner,user,payer
DualUSerMI:
    1 contract of type MOBILE with roles owner,user,payer
    1 contract of type INTERNET with roles owner,user,payer
DualUSerFI:
    1 contract of type FIX with roles owner,user,payer
    1 contract of type INTERNET with roles owner,user,payer
TripleUser:
    1 contract of type MOBILE with roles owner,user,payer
    1 contract of type FIX with roles owner,user,payer
    1 contract of type INTERNET with roles owner,user,payer

FamilyUser:   1 owner  i1 of 3 contrats, i2 , i3 users
    i1 :
        1 contract Mobile m1 with roles owner,user,payer
        1 contract Mobile m2 with roles owner,payer
        1 contract Mobile m3 with roles owner,payer
    i2 :
        1 contract Mobile m2 with roles user
    i3:
        1 contract Mobile m3 with roles user

# creation sequence

## CREATE_IDENTITY ( Gid )
create identity   i1 ( Gid: )

## ADD_CONTRACT ( gid , contractID, provider )

create contract  c1  ( id: , provider: )
    create roles associated to the contract
        c1.Owner , c1.User , c1.Payer

create contractLink l1
    link l1 with identity i1
    link l1 with c1.Owner , c1.Payer

## CHANGE_ROLE ( Gid:i2 , contractID:c1 ,role=User  )
create identity i2 ( Gid ) if not Done
creation contractLink l2
link l2 with identity i2
remove link to c1.User
link l2 with c1.User
