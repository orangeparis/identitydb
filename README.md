
# dgraph

*
* https://dgraph.io/
* https://dgraph.io/docs
* https://dgraph.io/docs/clients/go/
* https://dgraph.io/blog/post/introducing-entity-resolution/
* https://dgraph.io/blog/post/putting-it-all-together-part1/


#  quick start

see: https://dgraph.io/docs/get-started/

    docker run --rm -it -p 8080:8080 -p 9080:9080 -p 8000:8000 -v ~/dgraph:/dgraph dgraph/standalone:v20.11.0

* access Ratel at http://localhost:8080.
* tutorials : https://dgraph.io/docs/tutorials/



#  schema

type Indentity {
    gid : ID!
    hasContract: [ContractLink]  @hasInverse(field: UsedBy)  # link to contracts
}

type ContractLink {
    contractRef: Contract!  @hasInverse      # ref to contract
    role: [Role]                             # roles for this contract ( at list one )
}

type Contract {
    id: ID!                         # contract identifier
    provider: Provider!  @search(by: [hash])    # Provider of the contract ( Mobile / Fixe ...)
}

enum Provider {
  MOBILE
  FIXE
  INTERNET
}

enum Role {
    Owner
    User
    Payer
}