module bitbucket.org/orangeparis/identitydb

go 1.13

require (
	github.com/dgraph-io/dgo v1.0.0
	github.com/dgraph-io/dgo/v200 v200.0.0-20210125093441-2ab429259580
	google.golang.org/grpc v1.23.0
)
