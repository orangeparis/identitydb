package main

import (
	"context"
	"encoding/json"
	"log"

	//"github.com/dgraph-io/dgo"
	"github.com/dgraph-io/dgo/v200/protos/api"
	"google.golang.org/grpc"
)

func getDgraphClient() (dg api.DgraphClient, cancel context.CancelFunc) {

	ctx := context.Background()

	conn, err := grpc.Dial("localhost:9080", grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}
	dgraphClient := api.NewDgraphClient(conn)

	ctx, cancel = context.WithCancel(ctx)

	return dgraphClient, cancel

}

func main() {

	ctx := context.Background()

	dg, cancel := getDgraphClient()
	defer cancel()

	// conn, err := grpc.Dial("localhost:9080", grpc.WithInsecure())
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// defer conn.Close()
	// dgraphClient := dgo.NewDgraphClient(api.NewDgraphClient(conn))

	// Alter Shema

	op := &api.Operation{
		Schema: `name: string @index(exact) .`,
	}

	if resp, err := dg.Alter(ctx, op); err != nil {
		log.Fatal(err)
	}

	//  add data

	type Person struct {
		Uid   string   `json:"uid,omitempty"`
		Name  string   `json:"name,omitempty"`
		DType []string `json:"dgraph.type,omitempty"`
	}

	p := Person{
		Uid:   "_:alice",
		Name:  "Alice",
		DType: []string{"Person"},
	}

	pb, err := json.Marshal(p)
	if err != nil {
		log.Fatal(err)
	}

	mu := &api.Mutation{
		CommitNow: true,
	}
	mu.SetJson = pb

	response, err := dg.NewTxn().Mutate(ctx, mu)
	if err != nil {
		log.Fatal(err)
	}

	// response, err := txn.Mutate(ctx, mu)
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// txn.Discard(ctx)

	_ = response

}
